import java.util.Scanner

enum class Colors{
    ROJO, NARANJA, AMARILLO, VERDE, AÑIL, AZUL, VIOLETA
}
fun main(){
    val sc = Scanner(System.`in`)
    println("Escriu el color:")
    val input= sc.next()
    println(checkColor(input))
}

fun checkColor(input: String): Boolean{
    for (i in Colors.values()){
        if (input.uppercase() == i.toString()){
            return true
        }
    }
    return false
}