import java.util.Scanner
import kotlin.properties.Delegates

abstract class Question(open val enunciat: String, open val correct: Boolean){
    abstract fun checkResult(enunciat: String,correct: Boolean, input: String)
    abstract fun question()

}


 class  FreeTextQuestion(override var enunciat: String, override var correct: Boolean): Question(enunciat,correct){
        val questions= mutableListOf<String>(
            "Cuándo se produjo el descubrimiento de América?",
            "El famoso monumento Torre Eiffel, en qué ciudad está?",
            "Cuál es el infinitivo del verbo Canto ?"
        )
        val answers= mutableListOf<String>(
            "1492", "PARIS", "CANTAR"
        )
        lateinit var answer: String
        var pos by Delegates.notNull<Int>()

         override fun question(){
             pos=0
             for (i in questions){
                 if (i== enunciat){
                     break
                 }
                 pos++
             }
             println(enunciat)
             answer = answers[pos]
         }

         override fun checkResult(enunciat: String, correct: Boolean, input: String) {
             if (input.uppercase() == answer){
                 this.correct = true
             }
         }
 }
 class  MultipleChoiseQuestion(override var enunciat: String, override var correct: Boolean): Question(enunciat,correct){
     val questions= mutableListOf<String>(
         "Cuándo se produjo el descubrimiento de América?",
         "El famoso monumento Torre Eiffel, en qué ciudad está?",
         "Cuál es el infinitivo del verbo Canto ?"
     )
     val answers= mutableListOf<MutableList<String>>(
         mutableListOf("1495","1492","1487","1500"),
         mutableListOf("NEW YORK","PARIS","BERLIN","BRUSELAS"),
         mutableListOf("CANTABA", "CANTAR", "CANTARIA", "CANTARE")
     )
     lateinit var answer: String
     var pos by Delegates.notNull<Int>()

     override fun question() {
         pos=0
         for (i in questions){
             if (i== enunciat){
                 break
             }
             pos++
         }
         println(enunciat)
         for (i in answers[pos]){
             print("$i ")
         }
         answer = answers[pos][1]
     }
     override fun checkResult(enunciat: String, correct: Boolean, input: String) {
         if (input.uppercase() == answer){
             this.correct = true
         }
     }

 }


class Quiz(val joc: List<Question>, var numOfCorrect: Int){
    fun finalResult(){
        for (i in joc){
            if (i.correct){
                numOfCorrect++
            }
        }
        println("Numero de aciertos: $numOfCorrect")
    }
}

fun main(){
    val quiz= Quiz(
        listOf(
            FreeTextQuestion("Cuándo se produjo el descubrimiento de América?",false),
            MultipleChoiseQuestion("Cuándo se produjo el descubrimiento de América?",false),
            FreeTextQuestion("El famoso monumento Torre Eiffel, en qué ciudad está?",false),
            FreeTextQuestion("Cuál es el infinitivo del verbo Canto ?",false),
            MultipleChoiseQuestion("El famoso monumento Torre Eiffel, en qué ciudad está?",false),
            MultipleChoiseQuestion("Cuál es el infinitivo del verbo Canto ?",false)
        ),
        0
    )
    game(quiz)

}

fun game(quiz: Quiz){
    for (i in quiz.joc){
        val scanner= Scanner(System.`in`)
        i.question()
        i.checkResult(i.enunciat,i.correct,scanner.next())
    }
    quiz.finalResult()
}