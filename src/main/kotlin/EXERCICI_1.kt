import org.w3c.dom.Text

data class Student(val name: String, var textGrade: Grade)
enum class Grade{
    SUSPES,APROVAT,BE,NOTABLE,EXCELENT
}
fun main(){
    val firstStudent= Student("Carlos", Grade.SUSPES)
    val secondStudent= Student("Carla", Grade.EXCELENT)
    println(firstStudent)
    println(secondStudent)
}