
abstract class Instrument{
    abstract fun makeSounds(times: Int)
}
class Triangle(var intensitat: Int): Instrument(){
    init {
        if (checkIntensity() == false){
            intensitat=1
        }
    }
    override fun makeSounds(times: Int) {
        for (i in 1..times){
            so()
        }
    }
    fun so(){
        when(intensitat){
            1-> println(llistaIntensitat.TINC)
            2-> println(llistaIntensitat.TIINC)
            3-> println(llistaIntensitat.TIIINC)
            4-> println(llistaIntensitat.TIIIINC)
            5-> println(llistaIntensitat.TIIIIINC)
        }
    }
    fun checkIntensity():Boolean{
        for (i in llistaIntensitat.values()){
            if (intensitat == i.valor){
                return true
            }
        }
        return false
    }
}
class Drump(var to: String): Instrument(){

    init {
        if(!checkTo()){
            to = "A"
        }
    }
    override fun makeSounds(times: Int) {
        for (i in 1..times){
            so()
        }
    }
    fun so(){
        when(to){
            "A"-> println(typeOfTo.TAAAM)
            "O"-> println(typeOfTo.TOOOM)
            "U"-> println(typeOfTo.TUUUM)
        }
    }
    fun checkTo():Boolean{
        for (i in typeOfTo.values()){
            if (to.uppercase() == i.to){
                return true
            }
        }
        return false
    }
}
enum class typeOfTo(val to: String){
    TAAAM("A"),TOOOM("O"),TUUUM("U")
}
enum class llistaIntensitat(val valor: Int) {
    TINC(1),TIINC(2),TIIINC(3),TIIIINC(4),TIIIIINC(5)
}
fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}