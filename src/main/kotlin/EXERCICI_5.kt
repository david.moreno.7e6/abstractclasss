interface CarSensors{
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
}
enum class Direction{
    FRONT,LEFT,RIGHT
}
class AutonomousCar(): CarSensors{
    fun doNextNSteps(n :Int){
        for(i in 1..n){
            if (isThereSomethingAt(Direction.FRONT)){
                if (isThereSomethingAt(Direction.RIGHT)){
                    if (isThereSomethingAt(Direction.LEFT)){
                        stop()
                    }
                    else{
                        go(Direction.LEFT)
                    }
                }
                else{
                    go(Direction.RIGHT)
                }
            }
            else{
                go(Direction.FRONT)
            }
        }
    }
    override fun isThereSomethingAt(direction: Direction): Boolean {
        TODO("Comprova si hi ha un obstacle en una determinada direcció")
    }

    override fun go(direction: Direction) {
        TODO("avança en la direcció del parametre")
    }

    override fun stop() {
        TODO("Serveix per aturar el cotxe")
    }
}